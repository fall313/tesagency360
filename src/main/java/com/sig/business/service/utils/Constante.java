package com.sig.business.service.utils;

public class Constante {


    public static final String CODE_PAYS_FR = "Fr";
    public static final String SMS_ERRER_NOT_CREAT_ID_NOT_NULL_LISTING ="A new listingDto cannot already have an ID" ;
    public static final String SMS_ERRER_NOT_UPDATE_ID_NULL_LISTING ="Update listingDto  already have an ID" ;
    public static final String INFO_API_CREATE_LISTING = "Create a new listing";
    public static final String INFO_API_GET_LISTING ="get a listing by id." ;
    public static final String INFO_API_GET_DEALER_STATUT_PUBLISH =" Get all listings of a dealer with a given state equals publish." ;
    public static final String INFO_API_GET_DEALER_STATUT_DRAFT =" Get all listings of a dealer with a given state equals draft." ;
    public static final String INFO_API_PUBLISHED  ="publish a listing." ;
    public static final String INFO_API_DRAFT  ="draft a listing." ;
    public static final String SMS_NOT_FOUND = "listing not found";

    public static final String INFO_API_DELETE_LISTING = "delete the listing by id";
    public static final String INFO_API_LIST_LISTING = "Get all listings ";
    public static final String API_CREATE_LISTING = "/listing";
    public static final String API_DELETE_LISTING = "/listing/{id}";
    public static final String API_LIST_LISTING = "/listing";
    public static final String API_UPDATE_LISTING = "/listing";
    public static final String API_GET_LISTING = "/listing/{id}";
    public static final String API_PUBLISH = "/listing/publish/{id}";
    public static final String API_GET_BY_DEALER_PUBLISH = "/listing/dealer/publish/{idDealer}";
    public static final String API_GET_BY_DEALER_DRAFT = "/listing/dealer/draft/{idDealer}";
    public static final String API_DRAFT = "/listing/draft/{id}";
    public static final String API = "/api";
    public static final String INFO_API_UPDATE_LISTING = "Updates a listing.";
    public static final Object SMS_DELETE_SUCCESS = "delete has been successfully";
    public static final String SMS_ERRER_NOT_CREAT_MAX_PUBLISH = "You have reached the maximum number of linsting that you can post.";


    public static final String API_CREATE_DEALER = "/dealer";
    public static final String API_DELETE_DEALER = "/dealer/{id}";
    public static final String API_LIST_DEALER = "/dealer";
    public static final String API_UPDATE_DEALER = "/dealer";
    public static final String API_GET_DEALER = "/dealer/{id}";

    public static final String INFO_API_UPDATE_DEALER = "Updates a dealer.";

    public static final String INFO_API_CREATE_DEALER = "Create a new dealer";

    public static final String SMS_NOT_FOUND_DEALER = "listing not dealer";
    public static final String SMS_ERRER_NOT_CREAT_ID_NOT_NULL_DEALER = "A new dealer cannot already have an ID" ;
    public static final String INFO_API_GET_DEALER = "get a dealer by id." ;
    public static final String INFO_API_DELETE_DEALER = "delete the dealer by id";


    public static final String INFO_API_LIST_DEALER = "get all dealer";
    public static final String SMS_ERROR_MONTANT = "montant must be greater  0 ";
}
