package com.sig.business.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "listing")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class ListingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private long id;

    @Column(name = "vehicule", nullable = false)
    @NotNull(message = "name vehicule must be null")
    private String vehicule;

    @Column(name = "price", nullable = false)
    @NotNull(message = "price  must be null")
    private BigDecimal price;
    @Column(name = "creation_at", nullable = false)
    @NotNull(message = "creation date must be null")
    private LocalDate createdAt;
    /**
     * dealer
     */
    @NotNull(message = "dealer must be null")
    @ManyToOne
    private DealerEntity dealer    ;
    /**
     * enum
     * Statut ={ PUBLISHED, DRAFT }
     */
    @Column(name = "statut",length = 20)
    @Enumerated(EnumType.STRING)
    @NotNull(message = "gender is not null")
    private Statut statut;

}
