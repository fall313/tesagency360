package com.sig.business.imp;


import com.sig.business.ListingApplication;
import com.sig.business.repository.DealerRepository;
import com.sig.business.service.IDealerService;
import com.sig.business.service.dto.DealerDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test IDealerServiceImpl
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ListingApplication.class)
@Transactional
public class IDealerServiceImplTest {




    @Autowired
    private IDealerService service;

    @Autowired
    private DealerRepository dealerRepository;

    @Test
    public void testSave() {
        DealerDto dto=new DealerDto();
        dto.setName("France");
        dto=service.save(dto);
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isNotNull();
    }

    @Test
    public void testGetAll() {
        creatDealer("Serigne FALL");
        creatDealer("Elies");

        List<DealerDto> reponse=service.getAlldealer();

        assertThat(reponse).isNotNull();
        assertThat(reponse.size()>0 ).isTrue();
        assertThat(reponse.stream().map(DealerDto::getName).anyMatch(e-> e.equals("Elies")) ).isTrue();
    }

    private void creatDealer(String name) {
        DealerDto dto=new DealerDto();
        dto.setName(name);
        service.save(dto);
    }


}
