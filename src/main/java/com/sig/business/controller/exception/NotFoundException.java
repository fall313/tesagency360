package com.sig.business.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException  {
      public NotFoundException(Long id, String entity) {
            super("Could not found "+entity+ " " + id);
        }
}
