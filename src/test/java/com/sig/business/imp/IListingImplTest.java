package com.sig.business.imp;


import com.sig.business.ListingApplication;
import com.sig.business.domain.Statut;
import com.sig.business.service.IDealerService;
import com.sig.business.service.IListingService;
import com.sig.business.service.dto.DealerDto;
import com.sig.business.service.dto.ListingDto;
import com.sig.business.service.utils.Constante;
import com.sig.business.controller.dto.ListingRechercheDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test ListingServiceImpl
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ListingApplication.class)
@Transactional
public class IListingImplTest {


    private static final String DEFAULT_VEHICUL = "TOYOTA";
    private static final LocalDate DEFAULT_DATE = LocalDate.now();

    private static final BigDecimal DEFAULT_PRICE = BigDecimal.ONE;
    private static final String DEFAULT_DEALER_NAME = "Serigne FALL";

    @Autowired
    private IDealerService dealerService;


    @Autowired
    private IListingService service;


    public ListingDto creatDto(String vehicul, BigDecimal price, LocalDate date, String nameDealer) {


        DealerDto countryDto = new DealerDto();
        countryDto.setName(nameDealer);
        countryDto = dealerService.save(countryDto);

        ListingDto dto = new ListingDto();
        dto.setVehicule(vehicul);
        dto.setStatut(Statut.DRAFT);
        dto.setCreatedAt(date);
        dto.setDealerId(countryDto.getId());
        dto.setPrice(price);

        return dto;

    }


    @Test
    public void testSave() {
        ListingDto dto = creatDto(DEFAULT_VEHICUL, DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);

        ListingDto reponse = service.save(dto);
        assertThat(reponse).isNotNull();
        assertThat(reponse.getId()).isNotNull();
        assertThat(reponse.getVehicule()).isEqualTo(DEFAULT_VEHICUL);
        assertThat(reponse.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(reponse.getPrice()).isEqualTo(DEFAULT_PRICE);


    }

    @Test
    public void testUpdate() {
        ListingDto dto = creatDto(DEFAULT_VEHICUL, DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);

        ListingDto reponse = service.save(dto);

        reponse.setPrice(BigDecimal.valueOf(100));
        reponse = service.save(reponse);
        assertThat(reponse).isNotNull();
        assertThat(reponse.getPrice()).isEqualTo(BigDecimal.valueOf(100));

    }




    @Test
    public void testDelte() {
        ListingDto dto = creatDto(DEFAULT_VEHICUL, DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);

        ListingDto reponse = service.save(dto);

        int sizeBefor = service.getAllListing().size();
        service.deleteById(reponse.getId());
        int sizeAfter = service.getAllListing().size();
        assertThat(sizeAfter + 1).isEqualTo(sizeBefor);

    }


    @Test
    public void testGetAll() {
        ListingDto dto = creatDto(DEFAULT_VEHICUL, DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);

        ListingDto reponse = service.save(dto);

        int sizeListe = service.getAllListing().size();
        assertThat(sizeListe > 0).isTrue();

    }


    @Test
    public void test_GetById_ok() {
        ListingDto dto = creatDto(DEFAULT_VEHICUL, DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);

        dto = service.save(dto);

        ListingDto result = service.getById(dto.getId());

        assertThat(result).isNotNull();
        assertThat(result.getId()).isNotNull();
        assertThat(result.getVehicule()).isEqualTo(DEFAULT_VEHICUL);
        assertThat(result.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(result.getStatut()).isEqualTo(Statut.DRAFT);
    }

    @Test
    public void testGetById_with_exception_id_not_Foound() {

        try {
             service.getById(Long.MAX_VALUE);

        } catch (RuntimeException e) {
            assertThat(e.getMessage()).contains("Could not found listing "+Long.MAX_VALUE);
        }

    }

    @Test
    public void test_publish_or_draf_with_exception_id_not_Foound() {

        try {
            service.publishedOrDraf(Long.MAX_VALUE, Statut.PUBLISHED);

        } catch (RuntimeException e) {
            assertThat(e.getMessage()).contains("Could not found listing "+Long.MAX_VALUE);
        }

    }

    @Test
    public void test_publish_or_draf_with_exception_max_publish() {
        ListingDto dto = creatDto("voiture4", DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);
        dto = service.save(dto);


        try {
            service.publishedOrDraf(dto.getId(), Statut.PUBLISHED);

        } catch (RuntimeException e) {
            assertThat(e.getMessage()).contains(Constante.SMS_ERRER_NOT_CREAT_MAX_PUBLISH);
        }

    }

    @Test
    public void test_getListingByDealerAndStatut_ok() {
        ListingDto dto = creatDto("voiture4", DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);
        dto = service.save(dto);
        List<ListingDto> reponse =service.getListingByDealerAndStatut(new ListingRechercheDto(dto.getDealerId(), Statut.PUBLISHED));
        assertThat(reponse.size()).isEqualTo(0);

         reponse =service.getListingByDealerAndStatut(new ListingRechercheDto(dto.getDealerId(), Statut.DRAFT));
        assertThat(reponse.size()).isNotZero();


    }

    private ListingDto publierTroisListing() {
        ListingDto dto = creatDto("toto", DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);
        service.publishedOrDraf(dto.getId(), Statut.PUBLISHED);

        dto = creatDto("tata", DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);
        service.publishedOrDraf(dto.getId(), Statut.PUBLISHED);

         dto = creatDto("bmw", DEFAULT_PRICE, DEFAULT_DATE, DEFAULT_DEALER_NAME);
        service.publishedOrDraf(dto.getId(), Statut.PUBLISHED);
        return dto;
    }




}
