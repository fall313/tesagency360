package com.sig.business.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NotCreateException extends RuntimeException  {


      public NotCreateException(String sms, String entity) {
            super(entity+" not created :  "+ sms);
        }

}
