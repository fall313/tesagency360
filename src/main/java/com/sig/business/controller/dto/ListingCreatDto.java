package com.sig.business.controller.dto;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ListingCreatDto {


    private Long id=null;
    @NotBlank(message = "name vehicle must be null")
    @NotNull
    private String vehicule;

    @NotNull
    private Long dealerId;

    @NotNull(message = "price  must be null")
    private BigDecimal price;




}
