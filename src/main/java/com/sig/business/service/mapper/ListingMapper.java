package com.sig.business.service.mapper;



import com.sig.business.domain.ListingEntity;
import com.sig.business.service.dto.ListingDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Listing and its DTO ListingDTO.
 */
@Mapper(componentModel = "spring", uses = {DealerMapper.class})
public interface ListingMapper extends EntityMapper<ListingDto, ListingEntity> {


    @Mapping(source = "dealer.id", target = "dealerId")
    ListingDto toDto(ListingEntity entity);


    @Mapping(source = "dealerId", target = "dealer")
    ListingEntity toEntity(ListingDto dto);

    default ListingEntity fromId(Long id) {
        if (id == null) {
            return null;
        }
        ListingEntity entity = new ListingEntity();
        entity.setId(id);
        return entity;
    }
}
