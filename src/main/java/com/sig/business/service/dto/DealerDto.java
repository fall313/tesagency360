package com.sig.business.service.dto;


import com.sig.business.controller.dto.DealerCreatDto;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class DealerDto implements Serializable {

    private Long id;

    private String name;

    public DealerDto(DealerCreatDto dto){
        this.name= dto.getName();
    }


}
