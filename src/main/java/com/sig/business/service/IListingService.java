package com.sig.business.service;


import com.sig.business.domain.Statut;
import com.sig.business.service.dto.ListingDto;
import com.sig.business.controller.dto.ListingRechercheDto;

import java.util.List;

public interface IListingService {
    ListingDto save(ListingDto listingDto);

    List<ListingDto> getAllListing();

    void deleteById(Long id);

    ListingDto getById(Long id);

    ListingDto publishedOrDraf(Long id, Statut statut);


    List<ListingDto> getListingByDealerAndStatut(ListingRechercheDto dto);
}
