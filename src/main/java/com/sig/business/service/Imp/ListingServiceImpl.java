package com.sig.business.service.Imp;

import com.sig.business.domain.ListingEntity;
import com.sig.business.domain.Statut;
import com.sig.business.repository.ListingRepository;
import com.sig.business.service.IListingService;
import com.sig.business.service.dto.ListingDto;
import com.sig.business.service.mapper.ListingMapper;
import com.sig.business.service.utils.Constante;
import com.sig.business.controller.dto.ListingRechercheDto;
import com.sig.business.controller.exception.NotFoundException;
import com.sig.business.controller.exception.NotUpdateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class ListingServiceImpl implements IListingService {

    public static final String ENTITY = "listing";
    private final Logger log = LoggerFactory.getLogger(ListingServiceImpl.class);

    private final ListingRepository listingRepository;

    @Value("${business.nombre.max.listing}")
    private Long maxPublish;


    private final ListingMapper listingMapper;

    public ListingServiceImpl(ListingRepository listingRepository, ListingMapper listingMapper) {

        this.listingRepository = listingRepository;
        this.listingMapper = listingMapper;
    }

    /**
     * save a new listing
     *
     * @param listingDato
     * @return
     */
    @Override
    public ListingDto save(ListingDto listingDato) {
        log.debug("Request to save listingDato : {}", listingDato);
        ListingEntity entity = listingMapper.toEntity(listingDato);
        entity = listingRepository.save(entity);
        log.debug("===================  create listing OK ============================");
        return listingMapper.toDto(entity);
    }


    /**
     * find all listing
     *
     * @return
     */
    @Override
    public List<ListingDto> getAllListing() {
        return listingRepository.findAll()
            .stream()
            .map( listingMapper::toDto)
            .collect(Collectors.toList());
    }

    /**
     * delete listing by id
     *
     * @param id
     */
    @Override
    public void deleteById(Long id) {
        listingRepository.deleteById(id);
    }

    /**
     * get listing by id
     *
     * @param id
     * @return
     */
    @Override
    public ListingDto getById(Long id) {

        log.debug("Request to get listing : {}", id);
        ListingEntity entity = listingRepository.findById(id)
            .orElseThrow(() -> new NotFoundException(id, ENTITY));

        return listingMapper.toDto(entity);
    }

    @Override
    public ListingDto publishedOrDraf(Long id, Statut statut) {

        ListingDto dto = getById(id);

        if(checkIfNbrsPublishIsMaxByDealer(dto.getDealerId() , statut)){
            throw new NotUpdateException(Constante.SMS_ERRER_NOT_CREAT_MAX_PUBLISH);
        }
        dto.setStatut(statut);
        return save(dto);
    }


    private boolean checkIfNbrsPublishIsMaxByDealer(Long id, Statut statut) {
        return statut.equals(Statut.PUBLISHED) && listingRepository.getByDealerIdAndStatut(id,Statut.PUBLISHED).size() >= maxPublish;
    }

    @Override
    public List<ListingDto> getListingByDealerAndStatut(ListingRechercheDto dto) {
      return listingRepository.getByDealerIdAndStatut(dto.getDealerId(),dto.getStatut())
          .stream()
          .map(listingMapper::toDto).collect(Collectors.toList());
    }


}
