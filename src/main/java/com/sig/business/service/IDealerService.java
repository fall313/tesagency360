package com.sig.business.service;

import com.sig.business.service.dto.DealerDto;

import java.util.List;

public interface IDealerService {


    DealerDto save(DealerDto dealerDto);

    boolean existsById(Long id);

    DealerDto getById(Long id);

    void deleteById(Long id);

    List<DealerDto> getAlldealer();
}
