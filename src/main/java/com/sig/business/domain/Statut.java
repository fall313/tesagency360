package com.sig.business.domain;


public enum Statut {
    PUBLISHED, DRAFT;

}
