package com.sig.business.repository;

import com.sig.business.ListingApplication;
import com.sig.business.domain.DealerEntity;
import com.sig.business.domain.ListingEntity;
import com.sig.business.domain.Statut;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test  ListingRepository
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ListingApplication.class)
@Transactional
public class ListingRepositoryTest {

    @Autowired
    private ListingRepository repository;

    private ListingEntity advertisement;

    @Autowired
    private DealerRepository dealerRepository;

    public ListingEntity creat() {

         DealerEntity dealerEntity= new DealerEntity();
        dealerEntity.setName("Sengal");
        dealerEntity = dealerRepository.save(dealerEntity);

        advertisement = new ListingEntity();
        advertisement.setVehicule("TOYOTA");
        advertisement.setPrice(BigDecimal.ONE);
        advertisement.setCreatedAt(LocalDate.now());
        advertisement.setStatut(Statut.DRAFT);
        advertisement.setDealer(dealerEntity);

        return repository.save(advertisement);
    }

    @Before
    public void setUp() {
    }

    /**
     * Test find by id
     */
    @Test
    public void testFindById() {
        ListingEntity entity= creat();
        ListingEntity result = repository.findById(entity.getId()).orElse(null);
        assertThat(result).isNotNull();
        assertThat(result.getId()).isNotNull();
    }


    /**
     * Test find all
     */
    @Test
    public void testFindAll() {
        creat();
        List<ListingEntity> result = repository.findAll();
        assertThat(result).isNotNull();
        assertThat(result.size()).isNotZero();
    }

}
