package com.sig.business.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.constraints.NotNull;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class DealerNotFoundException extends RuntimeException  {

    public DealerNotFoundException(@NotNull Long dealerId) {
        super("Could not found dealer " + dealerId);
    }
}
