package com.sig.business.controller.dto;


import com.sig.business.domain.Statut;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

/**
 * S.FALL
 */

@Getter
@Setter
public class ListingRechercheDto {
    @NotNull
    private Long dealerId;
    @NotNull
    @Enumerated(EnumType.STRING)
    private Statut statut;


    public ListingRechercheDto(Long idDealer, Statut statut) {
        this.dealerId=idDealer;
        this.statut=statut;
    }
}
