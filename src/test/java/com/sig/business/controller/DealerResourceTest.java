package com.sig.business.controller;


import com.sig.business.ListingApplication;
import com.sig.business.service.IDealerService;
import com.sig.business.service.dto.DealerDto;
import com.sig.business.controller.dto.DealerCreatDto;
import com.sig.business.controller.dto.ListingCreatDto;
import lombok.Getter;
import lombok.Setter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for the dealerResourceIntTest REST controller.
 *
 * @see DealerResourceTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ListingApplication.class)
public class DealerResourceTest {


    @Autowired
    private IDealerService dealerService;


    private static final String DEFAULT_NAME = "serigne";


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;


    @Autowired
    private EntityManager em;

    private MockMvc restMockMvc;

    private ListingCreatDto ListingCreatDto;

    @Getter
    @Setter
    private DealerDto dealerDto = new DealerDto();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DealerResource resource = new DealerResource( dealerService);
        this.restMockMvc = MockMvcBuilders.standaloneSetup(resource)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                //  .setControllerAdvice(exceptionTranslator)
                // .setConversionService(createFormattingConversionService())
                .setMessageConverters(jacksonMessageConverter).build();
    }






    @Before
    public void initTest() {


    }

    @Test
    @Transactional
    public void test_api_create_ok() throws Exception {
        DealerCreatDto dto = new DealerCreatDto();
        dto.setName(DEFAULT_NAME);

        restMockMvc.perform(post("/api/dealer")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
                .andExpect(status().isCreated());


    }




    @Test
    @Transactional
    public void test_api_update_exception_id_is_null() throws Exception {


        restMockMvc.perform(put("/api/dealer")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(new DealerDto())))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void test_api_update_ok() throws Exception {


        DealerDto dto = initDealer();
        dto=dealerService.save(dto);

        dto.setName("Samba");


        restMockMvc.perform(put("/api/dealer")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isOk());


    }

    private static DealerDto initDealer() {
        DealerDto dto = new DealerDto();
        dto.setName(DEFAULT_NAME);
        return dto;
    }










    @Test
    @Transactional
    public void test_getById_ok() throws Exception {


        DealerDto dto=dealerService.save(initDealer());
        restMockMvc.perform(get("/api/dealer/{id}", dto.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(dto.getId().intValue()))
                .andExpect(jsonPath("$.name").value(dto.getName()));


    }

    @Test
    @Transactional
    public void test_getById_exception_not_found() throws Exception {

        restMockMvc.perform(get("/api/dealer/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());

    }


    @Test
    @Transactional
    public void test_api_getAll() throws Exception {
        // Initialize the database


        DealerDto dto=dealerService.save(initDealer());

        restMockMvc.perform(get("/api/dealer"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*].id").value(hasItem(dto.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(dto.getName())));




    }


    @Test
    @Transactional
    public void test_api_delete_is_ok() throws Exception {
        // Initialize the database


        DealerDto dto=dealerService.save(initDealer());
        int sizeBefore = dealerService.getAlldealer().size();

        restMockMvc.perform(delete("/api/dealer/{id}", dto.getId()))
                .andExpect(status().isOk());

        int sizeAfter = dealerService.getAlldealer().size();
        assertThat(sizeAfter + 1).isEqualTo(sizeBefore);


    }

    @Test
    @Transactional
    public void test_api_delete_is_no_content() throws Exception {

        restMockMvc.perform(delete("/api/dealer/{id}", Long.MAX_VALUE))
            .andExpect(status().isNoContent());


    }

}
