package com.sig.business.service.mapper;



import com.sig.business.domain.DealerEntity;
import com.sig.business.service.dto.DealerDto;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity DealerEntity and its DTO DealerDto.
 */
@Mapper(componentModel = "spring")
public interface DealerMapper extends EntityMapper<DealerDto, DealerEntity> {



    default DealerEntity fromId(Long id) {
        if (id == null) {
            return null;
        }
        DealerEntity entity = new DealerEntity();
        entity.setId(id);
        return entity;
    }
}
