package com.sig.business.repository;

import com.sig.business.domain.ListingEntity;
import com.sig.business.domain.Statut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingRepository extends JpaRepository<ListingEntity, Long>, JpaSpecificationExecutor<ListingEntity> {

    List<ListingEntity> getByDealerIdAndStatut(Long id, Statut statut);
}
