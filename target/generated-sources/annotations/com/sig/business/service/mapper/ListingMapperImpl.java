package com.sig.business.service.mapper;

import com.sig.business.domain.DealerEntity;
import com.sig.business.domain.ListingEntity;
import com.sig.business.service.dto.ListingDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-06-08T21:43:19+0000",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.14 (Oracle Corporation)"
)
@Component
public class ListingMapperImpl implements ListingMapper {

    @Autowired
    private DealerMapper dealerMapper;

    @Override
    public List<ListingEntity> toEntity(List<ListingDto> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ListingEntity> list = new ArrayList<ListingEntity>( dtoList.size() );
        for ( ListingDto listingDto : dtoList ) {
            list.add( toEntity( listingDto ) );
        }

        return list;
    }

    @Override
    public List<ListingDto> toDto(List<ListingEntity> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ListingDto> list = new ArrayList<ListingDto>( entityList.size() );
        for ( ListingEntity listingEntity : entityList ) {
            list.add( toDto( listingEntity ) );
        }

        return list;
    }

    @Override
    public ListingDto toDto(ListingEntity entity) {
        if ( entity == null ) {
            return null;
        }

        ListingDto listingDto = new ListingDto();

        listingDto.setDealerId( entityDealerId( entity ) );
        listingDto.setId( entity.getId() );
        listingDto.setVehicule( entity.getVehicule() );
        listingDto.setCreatedAt( entity.getCreatedAt() );
        listingDto.setPrice( entity.getPrice() );
        listingDto.setStatut( entity.getStatut() );

        return listingDto;
    }

    @Override
    public ListingEntity toEntity(ListingDto dto) {
        if ( dto == null ) {
            return null;
        }

        ListingEntity listingEntity = new ListingEntity();

        listingEntity.setDealer( dealerMapper.fromId( dto.getDealerId() ) );
        if ( dto.getId() != null ) {
            listingEntity.setId( dto.getId() );
        }
        listingEntity.setVehicule( dto.getVehicule() );
        listingEntity.setPrice( dto.getPrice() );
        listingEntity.setCreatedAt( dto.getCreatedAt() );
        listingEntity.setStatut( dto.getStatut() );

        return listingEntity;
    }

    private Long entityDealerId(ListingEntity listingEntity) {
        if ( listingEntity == null ) {
            return null;
        }
        DealerEntity dealer = listingEntity.getDealer();
        if ( dealer == null ) {
            return null;
        }
        long id = dealer.getId();
        return id;
    }
}
