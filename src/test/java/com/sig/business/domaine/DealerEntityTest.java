package com.sig.business.domaine;


import com.sig.business.domain.DealerEntity;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DealerEntityTest {


        @Test
        public void equalsVerifier()  {
            DealerEntity entity = new DealerEntity();
            entity.setId(1L);
            DealerEntity entity1 = new DealerEntity();
            entity1.setId(entity.getId());
            assertThat(entity).isEqualTo(entity1);
            entity1.setId(2L);
            assertThat(entity).isNotEqualTo(entity1);
            entity.setId(0L);
            assertThat(entity).isNotEqualTo(entity1);
        }


}
