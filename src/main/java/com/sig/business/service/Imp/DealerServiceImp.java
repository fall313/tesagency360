package com.sig.business.service.Imp;


import com.sig.business.domain.DealerEntity;
import com.sig.business.repository.DealerRepository;
import com.sig.business.service.IDealerService;
import com.sig.business.service.dto.DealerDto;
import com.sig.business.service.mapper.DealerMapper;
import com.sig.business.controller.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DealerServiceImp implements IDealerService {

    private final Logger log = LoggerFactory.getLogger(DealerServiceImp.class);

    private final DealerRepository dealerRepository;

    private final DealerMapper dealerMapper;



    public DealerServiceImp(DealerRepository dealerRepository, DealerMapper dealerMapper) {
        this.dealerRepository = dealerRepository;
        this.dealerMapper = dealerMapper;
    }


    @Override
    public DealerDto save(DealerDto dealerDto) {
        log.debug("Request to save DealerDto : {}", dealerDto);
        DealerEntity entity=dealerMapper.toEntity( dealerDto);
        entity=dealerRepository.save(entity);
        return dealerMapper.toDto(entity);
    }

    @Override
    public List<DealerDto> getAlldealer() {

        log.debug("Request to get all  dealer ");
        return dealerRepository.findAll().stream().map(dealerMapper::toDto).collect(Collectors.toList());

    }
    @Override
    public boolean existsById(Long id){
        return dealerRepository.existsById(id);
    }

    @Override
    public DealerDto getById(Long id) {
        log.debug("service to get dealer : {}", id);
        return dealerRepository.findById(id).map(dealerMapper::toDto).
            orElseThrow(() -> new NotFoundException(id, "dealer"));
    }

    @Override
    public void deleteById(Long id) {
        dealerRepository.deleteById(id);
    }


}
