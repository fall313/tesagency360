package com.sig.business.domaine;

import com.sig.business.domain.ListingEntity;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ListingEntityTest {


        @Test
        public void equalsVerifier()  {
            ListingEntity ob1 = new ListingEntity();
            ob1.setId(1L);
            ListingEntity ob2 = new ListingEntity();
            ob2.setId(ob1.getId());
            assertThat(ob1).isEqualTo(ob2);
            ob2.setId(2L);
            assertThat(ob1).isNotEqualTo(ob2);
            ob1.setId(0L);
            assertThat(ob1).isNotEqualTo(ob2);
        }


}
