package com.sig.business.repository;

import com.sig.business.ListingApplication;
import com.sig.business.domain.DealerEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test AdvertisementRepository
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ListingApplication.class)
@Transactional
public class DealerRepositoryTest {

    @Autowired
    private DealerRepository repository;

    private DealerEntity dealerEntity;

    public void createDealer() {

        dealerEntity = new DealerEntity();

        dealerEntity.setName("Serigne FALL");
        dealerEntity = repository.save(dealerEntity);
    }

    @Before
    public void setUp()  {
    }

    /**
     * Test find all Advertisement
     */
    @Test
    public void testFindALL() {
        createDealer();
        List<DealerEntity> result = repository.findAll();
        assertThat(result).isNotNull();
        assertThat(result.size()>=1).isTrue();
    }

}
