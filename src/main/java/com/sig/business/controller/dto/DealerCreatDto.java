package com.sig.business.controller.dto;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DealerCreatDto {

    @NotBlank(message = "name must be null")
    @NotNull
    private String name;



}
