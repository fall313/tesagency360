package com.sig.business.service.mapper;

import com.sig.business.domain.DealerEntity;
import com.sig.business.service.dto.DealerDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-06-08T21:43:18+0000",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.14 (Oracle Corporation)"
)
@Component
public class DealerMapperImpl implements DealerMapper {

    @Override
    public DealerEntity toEntity(DealerDto dto) {
        if ( dto == null ) {
            return null;
        }

        DealerEntity dealerEntity = new DealerEntity();

        if ( dto.getId() != null ) {
            dealerEntity.setId( dto.getId() );
        }
        dealerEntity.setName( dto.getName() );

        return dealerEntity;
    }

    @Override
    public DealerDto toDto(DealerEntity entity) {
        if ( entity == null ) {
            return null;
        }

        DealerDto dealerDto = new DealerDto();

        dealerDto.setId( entity.getId() );
        dealerDto.setName( entity.getName() );

        return dealerDto;
    }

    @Override
    public List<DealerEntity> toEntity(List<DealerDto> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<DealerEntity> list = new ArrayList<DealerEntity>( dtoList.size() );
        for ( DealerDto dealerDto : dtoList ) {
            list.add( toEntity( dealerDto ) );
        }

        return list;
    }

    @Override
    public List<DealerDto> toDto(List<DealerEntity> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<DealerDto> list = new ArrayList<DealerDto>( entityList.size() );
        for ( DealerEntity dealerEntity : entityList ) {
            list.add( toDto( dealerEntity ) );
        }

        return list;
    }
}
