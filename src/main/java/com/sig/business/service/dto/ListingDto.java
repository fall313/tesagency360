package com.sig.business.service.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.sig.business.domain.Statut;
import com.sig.business.controller.dto.ListingCreatDto;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ListingDto {
    private Long id;

    @NotBlank(message = "name vehicle must be null")
    @NotNull
    private String vehicule;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createdAt;
    @NotNull
    private Long dealerId;

    @NotNull(message = "price  must be null")
    private BigDecimal price;

	@Enumerated(EnumType.STRING)
	private Statut statut;

    public ListingDto(ListingCreatDto advertisementResourceDto){
        this.setPrice(advertisementResourceDto.getPrice());
        this.setDealerId(advertisementResourceDto.getDealerId());
        this.setVehicule(advertisementResourceDto.getVehicule());
        this.setCreatedAt(LocalDate.now());
        this.setStatut(Statut.DRAFT);
    }



}
