package com.sig.business.repository;

import com.sig.business.domain.DealerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DealerRepository extends JpaRepository<DealerEntity, Long>, JpaSpecificationExecutor<DealerEntity> {

}
