package com.sig.business.controller;


import com.sig.business.domain.Statut;
import com.sig.business.service.IDealerService;
import com.sig.business.service.IListingService;
import com.sig.business.service.dto.ListingDto;
import com.sig.business.service.utils.Constante;
import com.sig.business.controller.Utils.HeaderUtil;
import com.sig.business.controller.dto.ListingCreatDto;
import com.sig.business.controller.dto.ListingRechercheDto;
import com.sig.business.controller.exception.DealerNotFoundException;
import com.sig.business.controller.exception.NotCreateException;
import com.sig.business.controller.exception.NotFoundException;
import com.sig.business.controller.exception.NotUpdateException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing listing.
 */
@RestController
@Api(description = "API for CRUD  on listing")
@RequestMapping(Constante.API)
public class ListingResource {

    public static final String ENTITY_DEALER = "dealer";
    private final Logger log = LoggerFactory.getLogger(ListingResource.class);

    private static final String ENTITY_NAME = "listing";

    private final IListingService listingService;
    private final IDealerService dealerService;


    public ListingResource(IListingService listingService, IDealerService dealerService) {
        this.listingService = listingService;

        this.dealerService = dealerService;
    }

    /**
     * POST/listing : Create a new ListingDto.
     *
     * @param listingResourceDto the listingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new listingDto, or with status 400 (Bad Request) if the listing has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping(Constante.API_CREATE_LISTING)
    @ApiOperation(value = Constante.INFO_API_CREATE_LISTING)
    public ResponseEntity<ListingDto> creatlisting(@Valid @RequestBody ListingCreatDto listingResourceDto) throws URISyntaxException {
        ListingDto listingDto = new ListingDto(listingResourceDto);
        log.debug("REST request to save Listing : {}", listingDto);


        if (listingResourceDto.getId() != null) {
            throw new NotCreateException(Constante.SMS_ERRER_NOT_UPDATE_ID_NULL_LISTING,ENTITY_NAME );
        }
        if(listingDto.getPrice().compareTo(BigDecimal.ZERO)<=0){
            throw new NotCreateException(Constante.SMS_ERROR_MONTANT,ENTITY_NAME);
        }
        if (! dealerService.existsById(listingDto.getDealerId()) ) {
            throw new DealerNotFoundException(listingDto.getDealerId());
        }
        listingDto = listingService.save(listingDto);

        return ResponseEntity.created(new URI("/api/listing/" + listingDto.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, listingDto.getId().toString()))
                .body(listingDto);

    }


    @ApiOperation(value = Constante.INFO_API_UPDATE_LISTING)
    @PutMapping(Constante.API_UPDATE_LISTING)
    public ResponseEntity<ListingDto> updateListing(@Valid @RequestBody ListingDto listingDto)  {

        log.debug("REST request to update listingDto : {}", listingDto);
        if (listingDto.getId() == null) {
            throw new NotUpdateException(" Invalid id" + ENTITY_NAME + "idnull" );

        }
        if (! dealerService.existsById(listingDto.getDealerId()) ) {
            throw new NotFoundException(listingDto.getDealerId(),ENTITY_DEALER);
        }

        ListingDto result = listingService.save(listingDto);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);

    }


    /**
     * GET  /listing/:id : get the "id" listing.
     *
     * @param id the id of the listing to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the listing, or with status 404 (Not Found)
     */

    @ApiOperation(value = Constante.INFO_API_GET_LISTING)
    @GetMapping(Constante.API_GET_LISTING)
    //  @Timed
    public ResponseEntity<ListingDto> getListingById(@PathVariable Long id) {
        log.debug("REST request to get listing by id  : {}", id);

        return new ResponseEntity<>(listingService.getById(id), HttpStatus.OK);
    }

    @ApiOperation(value = Constante.INFO_API_PUBLISHED)
    @PutMapping(Constante.API_PUBLISH)
    public ResponseEntity<ListingDto> publish(@PathVariable Long id) {
        log.debug("REST request to publish listing by id  : {}", id);

        return new ResponseEntity<>(listingService.publishedOrDraf(id, Statut.PUBLISHED), HttpStatus.OK);
    }

    @ApiOperation(value = Constante.INFO_API_DRAFT)
    @PutMapping(Constante.API_DRAFT)
    public ResponseEntity<ListingDto> draft(@PathVariable Long id) {
        log.debug("REST request to draft listing by id  : {}", id);
        return new ResponseEntity<>(listingService.publishedOrDraf(id, Statut.DRAFT), HttpStatus.OK);
    }

    /**
     * DELETE/ delete the id listing
     *
     * @param id
     * @return
     */
    @ApiOperation(value = Constante.INFO_API_DELETE_LISTING)
    @DeleteMapping(Constante.API_DELETE_LISTING)
    public ResponseEntity<Object> deleteListingById(@PathVariable Long id) {
        log.debug("REST request to delete listing : {}", id);

        try {
            listingService.deleteById(id);
            return new ResponseEntity<>(Constante.SMS_DELETE_SUCCESS, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(Constante.SMS_NOT_FOUND, HttpStatus.NO_CONTENT);
        }

    }

    /**
     * PUT/listing Updates an existing listingDto.
     *
     * @param listingDto
     * @return
     * @throws URISyntaxException
     */


    /**
     * GET  /listing : get all the listing.
     *
     * @return
     */

    @ApiOperation(value = Constante.INFO_API_LIST_LISTING)
    @GetMapping(value = Constante.API_LIST_LISTING)
    public List<ListingDto> getAllListing() {
        log.debug("REST request to get all listing");
        return listingService.getAllListing();
    }

    @ApiOperation(value = Constante.INFO_API_GET_DEALER_STATUT_PUBLISH)
    @GetMapping(Constante.API_GET_BY_DEALER_PUBLISH)
    public List<ListingDto> getListingByDealerAndStatut(@PathVariable Long idDealer  ) {
        log.debug("REST request get all  listing by   : {}", idDealer);
        return listingService.getListingByDealerAndStatut(new ListingRechercheDto(idDealer,Statut.PUBLISHED));
    }

    @ApiOperation(value = Constante.INFO_API_GET_DEALER_STATUT_DRAFT)
    @GetMapping(Constante.API_GET_BY_DEALER_DRAFT)
    public List<ListingDto> getListingByDealer(@PathVariable Long idDealer  ) {
        log.debug("REST request get all  listing by   : {}", idDealer);
        return listingService.getListingByDealerAndStatut(new ListingRechercheDto(idDealer,Statut.DRAFT));
    }
}
