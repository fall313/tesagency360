# 360 Agency project

    ============= Serigne FALL =======================

## Description

Implement a REST service for managing listings for online advertising service.

    • Create a listing. All the created listings should have state draft by default;
    • Update a listing;
    • Get all listings of a dealer with a given state;
    • Publish a listing;
    • Unpublish a listing.

##IDE
    . IntelliJ IDEA 

## Installation
    . Lombok   
    . git version 2.20.1
    . Maven version 3.6.0    

## technologies and dependencies

✓ Java version: 8

✓ Packaging: Jar

✓ Dependencies

    • swagger

    • Spring Data JPA

    • Spring Web

    • H2 Database

    • Lombok

    . MapStruct

##Configuration H2 database

   > Saved Settings: Generic H2 (Embedded)

   > Setting Name: Generic H2 (Embedded)
 
   > Drive class : org.h2.Driver

   > JDBC URL : jdbc:h2:mem:businessdb
 
   > User Name: sa
 
   > password:
    

##Test Integrated H2 database

    .  http://localhost:8080/h2-console, this is the link that allows us to check the data stored in the database on the browser.###

## nombre max publish = 3

## Postman collection
=========== creation Status CREATED ====================== 

      {
        "dealerId": 1,
        "price": 100,
        "vehicule": "string"
    }

=========== creation Status NOT ACCEPTED ======================

      { 
        "dealerId": 10000,
        "price": 100,
        "vehicule": "string"
     }

  
=========== creation Status NOT BAD_REQUEST ====================== 

     ------- vehicule null
     {
        "dealerId": 10000,
        "price": 100,
        "vehicule": null
     }

##link Swagger

    . http://localhost:8080/swagger-ui.html 


## Lancement du projet : 
   mvn spring-boot:run

## Tests Unitaires
    mvn clean verify

## Les rapports Tests Unitaires & Sonar

  . lien rapports test d’intégration : https://gitlab.com/fall313/tesagency360/-/pipelines/894393890/test_report
  . lien analyse sonar : https://sonarcloud.io/summary/new_code?id=fall313_agency
  . lien conf pipeline : https://gitlab.com/fall313/tesagency360/-/blob/master/.gitlab-ci.yml

## Déploiement 

  . Lien swagger : http://192.162.70.216:9099/swagger-ui.html#/listing-resource