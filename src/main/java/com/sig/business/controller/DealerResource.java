package com.sig.business.controller;


import com.sig.business.service.IDealerService;
import com.sig.business.service.dto.DealerDto;
import com.sig.business.service.utils.Constante;
import com.sig.business.controller.Utils.HeaderUtil;
import com.sig.business.controller.dto.DealerCreatDto;
import com.sig.business.controller.exception.NotCreateException;
import com.sig.business.controller.exception.NotUpdateException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing dealer.
 */
@RestController
@Api(description = "API for CRUD  on dealer")
@RequestMapping(Constante.API)
public class DealerResource {

    private final Logger log = LoggerFactory.getLogger(DealerResource.class);

    private static final String ENTITY_NAME = "dealer";

    private final IDealerService dealerService;


    public DealerResource( IDealerService dealerService) {
        this.dealerService = dealerService;

    }

    /**
     * POST/dealer : Create a new dealerDto.
     *
     * @param dto the dealerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dealerDto, or with status 400 (Bad Request) if the dealer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping(Constante.API_CREATE_DEALER)
    @ApiOperation(value = Constante.INFO_API_CREATE_DEALER)
    public ResponseEntity<DealerDto> creatdealer(@Valid @RequestBody DealerCreatDto dto) throws URISyntaxException {
        log.debug("REST request to save dealer : {}", dto);

        DealerDto dealerDto= new DealerDto(dto);

        if (dealerDto.getId()!=null ) {
            throw new NotCreateException(Constante.SMS_ERRER_NOT_CREAT_ID_NOT_NULL_DEALER,ENTITY_NAME);
        }

        dealerDto = dealerService.save(dealerDto);

        return ResponseEntity.created(new URI("/api/dealer/" + dealerDto.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, dealerDto.getId().toString()))
                .body(dealerDto);

    }


    @ApiOperation(value = Constante.INFO_API_UPDATE_DEALER)
    @PutMapping(Constante.API_UPDATE_DEALER)
    public ResponseEntity<DealerDto> updatedealer(@Valid @RequestBody DealerDto dealerDto)  {

        log.debug("REST request to update dealerDto : {}", dealerDto);
        if (dealerDto.getId() == null) {
            throw new NotUpdateException(" Invalid id" + ENTITY_NAME + "idnull");
        }

        DealerDto result = dealerService.save(dealerDto);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);

    }


    /**
     * GET  /dealer/:id : get the "id" dealer.
     *
     * @param id the id of the dealer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dealer, or with status 404 (Not Found)
     */

    @ApiOperation(value = Constante.INFO_API_GET_DEALER)
    @GetMapping(Constante.API_GET_DEALER)
    //  @Timed
    public ResponseEntity<DealerDto> getdealerById(@PathVariable Long id) {
        log.debug("REST request to get dealer by id  : {}", id);

        return new ResponseEntity<>(dealerService.getById(id), HttpStatus.OK);
    }
    /**
     * DELETE/ delete the id dealer
     *
     * @param id
     * @return
     */
    @ApiOperation(value = Constante.INFO_API_DELETE_DEALER)
    @DeleteMapping(Constante.API_DELETE_DEALER)
    public ResponseEntity<Object> deletedealerById(@PathVariable Long id) {
        log.debug("REST request to delete dealer : {}", id);

        try {
            dealerService.deleteById(id);
            return new ResponseEntity<>(Constante.SMS_DELETE_SUCCESS, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(Constante.SMS_NOT_FOUND_DEALER, HttpStatus.NO_CONTENT);
        }

    }



    /**
     * GET  /dealer : get all the dealer.
     *
     * @return
     */

    @ApiOperation(value = Constante.INFO_API_LIST_DEALER)
    @GetMapping(value = Constante.API_LIST_DEALER)
    public List<DealerDto> getAlldealer() {
        log.debug("REST request to get all dealer");
        return dealerService.getAlldealer();
    }


}
