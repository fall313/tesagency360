package com.sig.business.controller;


import com.sig.business.ListingApplication;
import com.sig.business.domain.Statut;
import com.sig.business.service.IDealerService;
import com.sig.business.service.IListingService;
import com.sig.business.service.dto.DealerDto;
import com.sig.business.service.dto.ListingDto;
import com.sig.business.controller.dto.ListingCreatDto;
import lombok.Getter;
import lombok.Setter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for the ListingResourceIntTest REST controller.
 *
 * @see ListingResourceTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ListingApplication.class)
public class ListingResourceTest {


    @Autowired
    private IListingService listingService;

    @Autowired
    private IDealerService dealerService;


    private static final String DEFAULT_NAME = "toyota";


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;


    @Autowired
    private EntityManager em;

    private MockMvc restMockMvc;

    private ListingCreatDto ListingCreatDto;

    @Getter
    @Setter
    private DealerDto dealerDto = new DealerDto();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ListingResource listingResource = new ListingResource(listingService, dealerService);
        this.restMockMvc = MockMvcBuilders.standaloneSetup(listingResource)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                //  .setControllerAdvice(exceptionTranslator)
                // .setConversionService(createFormattingConversionService())
                .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public ListingCreatDto creatDto(String vehicul, BigDecimal price, Long idDealer) {

        ListingCreatDto dto = new ListingCreatDto();
        dto.setVehicule(vehicul);
        dto.setPrice(price);
        dto.setDealerId(idDealer);

        return dto;

    }

    public DealerDto creatDealerDto( String nameDealer) {

        DealerDto countryDto = new DealerDto();
        countryDto.setName(nameDealer);
        return dealerService.save(countryDto);

    }



    @Before
    public void initTest() {
        dealerDto = creatDealerDto("Serigne Fall");

    }

    @Test
    @Transactional
    public void test_api_create_ok() throws Exception {
        ListingCreatDto dto = creatDto("toyota",BigDecimal.ONE, dealerDto.getId() );

        restMockMvc.perform(post("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
                .andExpect(status().isCreated());
    }

    @Test
    @Transactional
    public void test_api_create_id_not_null() throws Exception {
        ListingCreatDto dto = creatDto("toyota",BigDecimal.ONE, dealerDto.getId() );
        dto.setId(12L);

        restMockMvc.perform(post("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isBadRequest());


    }

    @Test
    @Transactional
    public void test_api_create_exception_montant_zero() throws Exception {
        ListingCreatDto dto = creatDto("toyota",BigDecimal.ZERO, dealerDto.getId() );

        restMockMvc.perform(post("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isBadRequest());

    }

    @Test
    @Transactional
    public void test_api_create_exception_id_not_null() throws Exception {
        ListingCreatDto dto = creatDto("toyota",BigDecimal.ZERO, dealerDto.getId() );
        dto.setId(122L);

        restMockMvc.perform(post("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isBadRequest());

    }

    @Test
    @Transactional
    public void test_api_create_exception_montant_negatif() throws Exception {
        ListingCreatDto dto = creatDto("toyota",BigDecimal.valueOf(-10), dealerDto.getId() );

        restMockMvc.perform(post("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isBadRequest());

    }

    @Test
    @Transactional
    public void test_api_create_exception_dealer_not_exist() throws Exception {
        ListingCreatDto dto = creatDto("toyota",BigDecimal.valueOf(10), dealerDto.getId() );
        dto.setDealerId(Long.MAX_VALUE);
        restMockMvc.perform(post("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isNotAcceptable());

    }

    @Test
    @Transactional
    public void test_api_create_exception_dealer_not_found() throws Exception {


        ListingCreatDto dto = creatDto("toyota",BigDecimal.ONE, dealerDto.getId() );
        dto.setDealerId(Long.MAX_VALUE);

        restMockMvc.perform(post("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isNotAcceptable());


    }


    @Test
    @Transactional
    public void test_api_update_exception_id_is_null() throws Exception {

        ListingDto dto = listingService.save(new ListingDto(creatDto("toyota", BigDecimal.ONE, dealerDto.getId())));

        dto.setId(null);
        restMockMvc.perform(put("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void test_api_update_ok() throws Exception {


        ListingDto dto = listingService.save(new ListingDto(creatDto("toyota", BigDecimal.ONE, dealerDto.getId())));

        dto.setPrice(BigDecimal.valueOf(10));


        restMockMvc.perform(put("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isOk());


    }

    @Test
    @Transactional
    public void test_api_publish_ok() throws Exception {
        ListingDto dto = listingService.save(new ListingDto(creatDto("toyota", BigDecimal.ONE, dealerDto.getId())));
        restMockMvc.perform(put("/api/listing/publish/{id}",dto.getId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(dto.getId().intValue()))
            .andExpect(jsonPath("$.vehicule").value(dto.getVehicule()))
            .andExpect(jsonPath("$.price").value(BigDecimal.ONE.toString()))
            .andExpect(jsonPath("$.statut").value(Statut.PUBLISHED.toString()));
    }

    @Test
    @Transactional
    public void test_api_publish_exception_max_publish() throws Exception {
        publierTroisListing();
        ListingDto dtoQuatriemeListing = listingService.save(new ListingDto(creatDto("toyota", BigDecimal.ONE, dealerDto.getId())));

        restMockMvc.perform(put("/api/listing/publish/{id}",dtoQuatriemeListing.getId()))
            .andExpect(status().isBadRequest());
    }

    private ListingDto publierTroisListing() {
        ListingDto dto = listingService.save(new ListingDto(creatDto("toyota", BigDecimal.ONE, dealerDto.getId())));
        listingService.publishedOrDraf(dto.getId(), Statut.PUBLISHED);

        dto = listingService.save(new ListingDto(creatDto("tata", BigDecimal.ONE, dealerDto.getId())));
        listingService.publishedOrDraf(dto.getId(), Statut.PUBLISHED);

        dto = listingService.save(new ListingDto(creatDto("bmw", BigDecimal.ONE, dealerDto.getId())));
        listingService.publishedOrDraf(dto.getId(), Statut.PUBLISHED);
        return dto;
    }


    @Test
    @Transactional
    public void test_api_publish_exception_id_not_found() throws Exception {
        restMockMvc.perform(put("/api/listing/publish/{id}",Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }


    @Test
    @Transactional
    public void test_api_draft_ok() throws Exception {
        ListingDto dto = listingService.save(new ListingDto(creatDto("toyota", BigDecimal.ONE, dealerDto.getId())));
       listingService.publishedOrDraf(dto.getId(), Statut.PUBLISHED);

        restMockMvc.perform(put("/api/listing/draft/{id}",dto.getId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(dto.getId().intValue()))
            .andExpect(jsonPath("$.vehicule").value(dto.getVehicule()))
            .andExpect(jsonPath("$.price").value(BigDecimal.ONE.toString()))
            .andExpect(jsonPath("$.statut").value(Statut.DRAFT.toString()));
    }


    @Test
    @Transactional
    public void test_api_draft_exception_id_not_found() throws Exception {
        restMockMvc.perform(put("/api/listing/publish/{id}",Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }



    @Test
    @Transactional
    public void test_api_update_exception_dealer_not_found() throws Exception {


        ListingDto dto = listingService.save( new ListingDto(creatDto("toyota",BigDecimal.ONE, dealerDto.getId())));

        dto.setDealerId(Long.MAX_VALUE);

        restMockMvc.perform(put("/api/listing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
            .andExpect(status().isNotFound());


    }



    @Test
    @Transactional
    public void test_getById_ok() throws Exception {

        ListingDto dto = listingService.save( new ListingDto(creatDto("toyota",BigDecimal.ONE, dealerDto.getId())));

        restMockMvc.perform(get("/api/listing/{id}", dto.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(dto.getId().intValue()))
                .andExpect(jsonPath("$.vehicule").value(dto.getVehicule()))
                .andExpect(jsonPath("$.price").value(BigDecimal.ONE.toString()));

    }

    @Test
    @Transactional
    public void test_getById_exception_not_found() throws Exception {

        restMockMvc.perform(get("/api/listing/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());

    }


    @Test
    @Transactional
    public void test_api_getAll() throws Exception {
        ListingDto dto = listingService.save( new ListingDto(creatDto("toyota",BigDecimal.ONE, dealerDto.getId())));

        restMockMvc.perform(get("/api/listing"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*].id").value(hasItem(dto.getId().intValue())))
                .andExpect(jsonPath("$.[*].vehicule").value(hasItem(dto.getVehicule())))
                .andExpect(jsonPath("$.[*].price").value(hasItem(dto.getPrice().intValue())));
    }


    @Test
    @Transactional
    public void test_api_delete_is_ok() throws Exception {
        // Initialize the database


        ListingDto dto = listingService.save( new ListingDto(creatDto("toyota",BigDecimal.ONE, dealerDto.getId())));


        int sizeBefore = listingService.getAllListing().size();

        restMockMvc.perform(delete("/api/listing/{id}", dto.getId()))
                .andExpect(status().isOk());

        int sizeAfter = listingService.getAllListing().size();
        assertThat(sizeAfter + 1).isEqualTo(sizeBefore);


    }

    @Test
    @Transactional
    public void test_api_delete_is_no_content() throws Exception {

        restMockMvc.perform(delete("/api/listing/{id}", Long.MAX_VALUE))
            .andExpect(status().isNoContent());


    }


    @Test
    @Transactional
    public void test_api_getListingByDealerAndStatut_equals_publish_ok() throws Exception {

        ListingDto dto = listingService.save( new ListingDto(creatDto("toyota",BigDecimal.ONE, dealerDto.getId())));
       listingService.publishedOrDraf(dto.getId(), Statut.PUBLISHED);
        restMockMvc.perform(get("/api/listing/dealer/publish/{idDealer}", dto.getDealerId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasItem(dto.getId().intValue())))
            .andExpect(jsonPath("$.[*].vehicule").value(hasItem(dto.getVehicule())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(dto.getPrice().intValue())));

    }

    @Test
    @Transactional
    public void test_api_getListingByDealerAndStatut_equals_draft_ok() throws Exception {

        ListingDto dto = listingService.save( new ListingDto(creatDto("toyota",BigDecimal.ONE, dealerDto.getId())));

        restMockMvc.perform(get("/api/listing/dealer/draft/{idDealer}", dto.getDealerId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasItem(dto.getId().intValue())))
            .andExpect(jsonPath("$.[*].vehicule").value(hasItem(dto.getVehicule())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(dto.getPrice().intValue())));

    }




}
